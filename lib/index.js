'use strict';

var path = require('path');

module.exports = {
	contacts: require(path.join(__dirname, 'contacts')),
	files: require(path.join(__dirname, 'files')),
	templates: require(path.join(__dirname, 'templates'))
};
