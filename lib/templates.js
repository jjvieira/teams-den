'use strict';

var doT = require('dot');
var fs = require('fs');
var path = require('path');
var folderPath = path.join(__dirname, '..', 'templates');

// Define some doT overrides
doT.templateSettings.strip = false;

function load(name) {
	var content = fs.readFileSync(path.join(folderPath, name), 'utf8');

	return doT.template(content);
}

function render(name, data) {
	var doTEngine = load(name);

	return doTEngine(data);
}

module.exports = {
	render: render
};
