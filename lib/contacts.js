'use strict';

var EOL = require('os').EOL;
var doT = require('dot');
var base64Img = require('base64-img');
var forOwn = require('mout/object/forOwn');
var pick = require('mout/object/pick');
var arrayContains = require('mout/array/contains');
var isEmpty = require('mout/lang/isEmpty');
var deepClone = require('mout/lang/deepClone');
var slugify = require('mout/string/slugify');
var properCase = require('mout/string/properCase');
var path = require('path');
var filesUtil = require(path.join(__dirname, 'files'));
var templatesUtil = require(path.join(__dirname, 'templates'));
var config = require(path.join(__dirname, '..', 'config', 'index.js'));
var peopleFolderPath = path.join(__dirname, 'people');
var exportsFolderPath = path.join(__dirname, '..', 'exports', 'contacts');

function getPeople() {
	var people = [],
			files = filesUtil.readdirSync(peopleFolderPath);

	for (var i in files) {
		var fullPath = path.join(peopleFolderPath, files[i]);

		// Ignore 'johndoe' as it is only an example
		if (fullPath.indexOf('johndoe') > -1) {
			continue;
		}

		// If is directory, save it's name, as it represents the team member name
		if (filesUtil.statSync(fullPath).isDirectory()) {
			people.push(fullPath.split(path.sep).pop());
		}
	}

	return people;
}

function generateVCard(handler, done) {
	console.log('Creating vCard for [' + handler + ']...');

	// Get member details
	var avatarPath = path.join(__dirname, 'people', handler, 'avatar.jpg');
	var info = require(path.join(__dirname, 'people', handler, 'info.json'));

	// Build vCard data object
	var vCardData = {
		last_name: info.name.split(' ').pop(),
		first_name: info.name.split(' ').shift(),
		name: info.name,
		company_name: config.company_name,
		contacts_group: config.contacts_group,
		email_work: info.contact.email_work,
		email_alternative: info.contact.email_alternative,
		cellphone: info.contact.cellphone,
		landline: info.contact.landline,
		location: info.location,
		birthdate: info.birthdate,
		base64_avatar: null, // Should be a base64 value of the member avatar
		notes: null          // Should be values separated by '\n' character
	};

	// Build vCard notes (all other extra info hidding restricted keys from vCard)
	var restrictedKeys = ['ssh_public_key'],
			extraNotes = [];

	forOwn(info.extra, function(detail_nodes, parent_key) {
		extraNotes.push('\\n');

		forOwn(detail_nodes, function(v, k) {
			if (! arrayContains(restrictedKeys, k)) {
				if (v) {
					// extraNotes.push(parent_key + '.' + k + ': ' + v + '\\n');
					extraNotes.push(k + ': ' + v + '\\n');
				}
			}
		});
	});

	vCardData.notes = extraNotes.join('');

	// Asynchronously Build vCard avatar base64 image
	base64Img.base64(avatarPath, function(err, data) {
		if (err) {
			throw err;
		}

		// We have to extract the header "data:image/jpg;base64,"
		// as vCard do not recognize that
		vCardData.base64_avatar = data.substring(data.indexOf(',') + 1);

		// Write vCard to final destination
		filesUtil.writeFile(path.join(exportsFolderPath, handler + '.vcf'), templatesUtil.render('vcard.vcf.tmpl', vCardData), 'utf8', function(err, data) {
			if (err) {
				throw err;
			}

			// If all went well, return success
			done(null);
		});
	});
}

function compileVCards() {
	console.log('Compiling all vCard for team [' + config.contacts_group + ']...');

	var fullTeamVCardName = 'fullteam.vcf',
			fullTeamVCardPath = path.join(exportsFolderPath, fullTeamVCardName),
			files = filesUtil.readdirSync(exportsFolderPath),
			vCards = [];

	// Fetch all vCard to be compiled
	for (var i in files) {
		if (files[i].indexOf('.vcf') > -1 && files[i] !== fullTeamVCardName) {
			vCards.push(path.join(exportsFolderPath, files[i]));
		}
	}

	if (isEmpty(vCards)) {
		throw new Error('You cannot compile cards when there are no cards to compile.');
	}

	// If team full file already exists, remove it prior to compiling the new vCards
	filesUtil.removeIfExistsSync(fullTeamVCardPath);

	// Compile all cards
	for (var i in vCards) {
		filesUtil.appendFromSync(vCards[i], fullTeamVCardPath, 'utf8');
	}
}

function generateTeamHTML(people, done) {
	var fullTeamHTMLPage = slugify('meet-team-' + config.contacts_group) + '.html';
	console.log('Generating HTML page [' + fullTeamHTMLPage + '] for team [' + config.contacts_group + ']...');

	// TODOAQUI: falta fazer isto obviamente!!! Fazer LOAD a todos os team members!!! e gerar as imagens em base 64
	var htmlData = {
		company_name: config.company_name,
		team: [],
		helpers: {
			propertyToLabelText: function (json_node) {
				json_node = json_node.replace(/_/g, ' ');

				return properCase(json_node) + ':';
			},
			pick: pick
		}
	};

	people.forEach(function (handler, i) {
		htmlData.team.push({
			id: handler,
			value: deepClone(require(path.join(__dirname, 'people', handler, 'info.json')))
		});
	});

	// Write HTML file to final destination
	filesUtil.writeFile(path.join(exportsFolderPath, fullTeamHTMLPage), templatesUtil.render('team.html.tmpl', htmlData), 'utf8', function(err, data) {
		if (err) {
			throw err;
		}

		// If all went well, return success
		done(null);
	});
}

function showOperationResume() {
		console.log(EOL);
		console.log('==================================================================================================');
		console.log('!! DONE !!');
		console.log('==================================================================================================');
		console.log('Now, proceed to the `exports` folder namely the `contacts` sub folder.');
		console.log('==================================================================================================');
		console.log('REMINDER:');
		console.log('Before you import the vCard into a MAC OSx, please create the group `' + config.contacts_group + '` in your contacts.');
		console.log('If you failed to do so, you will have to manually add the vCard(s) to that group afterwards.');
		console.log('==================================================================================================');
		console.log('Cheers.');
		console.log('==================================================================================================');
}

module.exports = {
	getPeople: getPeople,
	generateVCard: generateVCard,
	compileVCards: compileVCards,
	generateTeamHTML: generateTeamHTML,
	showOperationResume: showOperationResume
};
