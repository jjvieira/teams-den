'use strict';

var EOL = require('os').EOL;
var fs = require('fs');

function fileExistsSync(file_path) {
	try {
		return fs.statSync(file_path).isFile();
	}
	catch (err) {
		return false;
	}
}

function removeIfExistsSync(file_path) {
	if (fileExistsSync(file_path)) {
		fs.unlinkSync(file_path);
	}
}

function appendFromSync(source_file_path, dest_file_path, options) {
	var content = fs.readFileSync(source_file_path, options);
	fs.appendFileSync(dest_file_path, content + EOL + EOL, options);
}

module.exports = {
	// Built-in
	readdirSync: fs.readdirSync,
	statSync: fs.statSync,
	writeFile: fs.writeFile,
	readFileSync: fs.readFileSync,
	appendFileSync: fs.appendFileSync,
	// Custom
	removeIfExistsSync: removeIfExistsSync,
	appendFromSync: appendFromSync
};
