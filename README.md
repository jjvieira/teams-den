# Team's Den

The idea behind this repository is to provide you with useful tools to get to know team members and also fetch common information about them that can be useful to you at any moment.

## Rationale

So, imagine that you're in a company and a new guy joins the team. You need his personal data to add to your contacts list, your Google contacts, your Mac OS X contacts, you name it. So, instead of having to create the contact yourself, why not use something that will provide that to you.

Even worse, imagine that **you're the new guy**, and you have to add all, let's say 30 members of your team to your contacts. Yaicks! Imagine the time you'll spend with that task.

Having this said, this tool will allows you to generate **vCards** for individual team members that you can easly import to your own personal contacts, like Google contacts, or Mac OS X Contacts. As said before, if you're the new guy, a `full team vCard` will also be created, thus allowing you to import all contacts at once. It also provides you with single vCards if you only need to import some of the contacts.

If you wish to take a look to your team, you can also open the _html_ page that this tool generates with all the team members identified by their data and avatar. Just don't expect a fancy design, at least not at this stage. As the other guy would say: Just works!

## How to run the tool

As this tool is made is `nodejs` you will need to install it. Steps to do that, can be found [here](https://nodejs.org/en/).

After having node installed, you have to run:

> npm run setup

This will create the `config/index.js` file that you will need to edit with your company details and the name of the group you want the contacts to be placed in when imported. Note that you can also do this by hand.

After that, you have to install all project dependencies, so run

> npm install

If you want to see the available npm commands, just type

> npm run

## How to setup your team and export contacts

As said before, this assumes that you have a team to work with, and that team is part of a company that you already configured.

So, assuming that you cloned this repo to your machine, you just need to:

  - Place yourself in `lib/people` folder and start creating your team. Just **copy the _johndoe_ folder** and take it as an example to start adding your team members. Ideally, each person would fill his own profile, in a shared repository within your team.
  - The avatar picture should respect the **`avatar.jpg`** name and extension and be placed alongside with the **`info.json`** file and should have a decent and moderate resolution. The advised resolution is around 500x500.

After creating the team you need to run:

> npm run export-contacts

and then, go the `exports/people` folder and you'll have everything you need there. Enjoy!

### NOTES

- If you're importing contacts to a **MAC OS X** system, please **create the group, as you defined in config file, pior to** importing contacts. If you don't, you will have do add them, by hand, afterwards. If you do, import will work perfectly.

- Don't forget, the first time you clone this repo, to edit the `.gitignore` file in the directory root so you allow your team to be commited. By default, that is not allowed. Only the johndoe is being commited so you can have a starting point to start defining your team.

## Roadmap

Eventually, this repository will hold some new exports or useful features that you can dynamically create from each team member definition.

## Tests

At this moment, no tests were implemented, as tests are done importing things into final destinations.
