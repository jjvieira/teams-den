'use strict';

var path = require('path');
var EOL = require('os').EOL;
var lib = require(path.join(__dirname, 'lib'));
var config = require(path.join(__dirname, 'config', 'index.js'));

switch(process.env.ACTION) {

	case 'export-contacts':
		// Get people to export
		var people = lib.contacts.getPeople(),
				processed = 0,
				htmlOk = false;

		// Generate an HTML page to display all team members
		lib.contacts.generateTeamHTML(people, function (err) {
			if (err) {
					throw err;
				}

				htmlOk = true;
		});

		// For each people, generate the vCards
		people.forEach(function (handler, i) {
			lib.contacts.generateVCard(handler, function (err) {
				if (err) {
					throw err;
				}

				processed++;
			});
		});

		// Things to do when all cards are generated
		var si = setInterval(function () {
			if (processed === people.length && htmlOk === true) {
				clearInterval(si);

				// Compile all vCards into just one for direct import if that is what you like
				lib.contacts.compileVCards();

				lib.contacts.showOperationResume();

				process.exit();
			}
		}, 500);
	break;

	default:
		console.log(EOL + 'Error: `action` not recognized.' + EOL);
		process.exit();

}